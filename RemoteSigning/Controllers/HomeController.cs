﻿using RemoteSigning.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO.Compression;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Protocols;
using System.Xml;
using Paperless.DotNetSdk.EnrollmentServiceRef;
using Paperless.DotNetSdk.SigningServiceRef;
using Paperless.DotNetSdk.SignatureProvider;
using Paperless.DotNetSdk;
using Paperless.DotNetSdk.EnrollmentData;
using Antlr.Runtime;
using System.Text.RegularExpressions;
using System.Text;
using iTextSharp.text.pdf;
using iTextSharp.text;
using RemoteSigning.Helpers;

namespace RemoteSigning.Controllers
{
    public class HomeController : Controller
    {
        public static string conString;

        public object SignatureTypeEnum { get; private set; }

        public ActionResult Index()
        {
            return RedirectToAction("Index", "Login");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Login()
        {
            ViewBag.Message = "Login";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Enrolment()
        {
            ViewBag.Message = "Enrolment";

            return View();
        }

        public ActionResult SignNew()
        {
          
            ViewBag.Message = "SignNew";
            Session["dok_uploads"] = null;

            Session["mesazhi_fillimi"] = "";
            string externalid = Session["ExternalId"] as String;
            if (externalid == "")
            {
                RedirectToAction("Index", "Login");
            }

            //externalid = "12345";

            SigningService.SigningService TermsAndConditions = new SigningService.SigningService();

            TermsAndConditions.ClientCertificates.Add(GetCert());
            try
            {
                var sth = TermsAndConditions.GetGeneralTermsAndConditions(externalid);
                byte[] byteArray = sth.FileTermsAndConditions;
                Session["HashTermsAndConditions"] = sth.HashTermsAndConditions;
                TC doc = new TC();
                doc.Data = byteArray;
                doc.type = "0";
                return View(doc);




            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Login");
            }
       
        }

        public ActionResult ChangePass()
        {
            ViewBag.Message = "ChangePass";
            string externalid = Session["ExternalId"] as String;
            using (Models.RNE2 dc = new Models.RNE2())
            {
                try
                {
                    
                    var v = (from person in dc.RNE_Person
                             join enrolment in dc.RNE_Enrolment.Where(enrolment => enrolment.externalid == externalid) on person.IDPerson equals enrolment.IDPerson
                             select new { RNE_Enrolment = enrolment }).FirstOrDefault();

                    if (v != null)
                    {
                        v.RNE_Enrolment.Password = "";
                        Session["ExternalId"] = v.RNE_Enrolment.externalid;
                        return View(v.RNE_Enrolment);
                    }
                    else
                    {
                        ViewBag.Message = "Wrong username or password";
                        return View();
                    }

                }
                catch
                {
                    ViewBag.ErrorMessage = "Error";
                    return View();
                }
            }
          
        }

        public ActionResult Sign()
        {
            ViewBag.Message = "Sign";
            //string certPath = "C:\\" + "sslroot.cer";
            string certPath = "C:\\" + "sslroot.cer";
            string externalid = Session["ExternalId"] as String;

     
            //externalid = "12345";
         
           SigningService.SigningService TermsAndConditions = new SigningService.SigningService();

           TermsAndConditions.ClientCertificates.Add(GetCert());
           var sth = TermsAndConditions.GetGeneralTermsAndConditions(externalid);
           byte[] byteArray = sth.FileTermsAndConditions;
           Session["HashTermsAndConditions"] = sth.HashTermsAndConditions;
           TC doc = new TC();
           doc.Data = byteArray;
           doc.type = "0";
           


          return View(doc);
        }

        /*
        [HttpPost]
        public ActionResult Enrolment(Enrolment enrolment)
        {
            string certPath = "C:\\" + "sslroot.cer";
            //public Dictionary<string, string> AdditionalInfo;

            EnrollmentService.EnrollmentService objEnrolRef = new EnrollmentService.EnrollmentService();
            EnrollmentService.EnrolmentRequest request = new EnrollmentService.EnrolmentRequest();
            request.ExternalId = enrolment.ExternalId;
            request.Cnp = enrolment.Cnp;
            request.IdCardSerial = enrolment.IdCardSerial;
            request.IdCardNumber = enrolment.IdCardNumber;
            request.IdCardIssueDate = enrolment.IdCardIssueDate;
            request.IdCardIssuer = enrolment.IdCardIssuer;
            request.IdCardExpirationDate = enrolment.IdCardExpirationDate;
            request.FirstName = enrolment.FirstName;
            request.LastName = enrolment.LastName;
            request.CustomerIdPhoto = enrolment.CustomerIdPhoto;
            request.PhoneNumber = enrolment.PhoneNumber;
            request.Email = enrolment.Email;
            request.Country = enrolment.Country;
            request.Sector = enrolment.Sector;
            request.Street = enrolment.Street;
            request.ZIPCode = enrolment.ZIPCode;
            //request.Profile = enrolment.Profile;
            request.Roles = enrolment.Roles;
            request.Nr = enrolment.Nr;
            request.Locality = enrolment.Locality;
            //request.Group = enrolment.Group;
            request.District = enrolment.District;
            request.Apartment = enrolment.Apartment;
            request.Address = enrolment.Address;

      

            //request.CertificateValidity = enrolment.CertificateValidity;
            objEnrolRef.ClientCertificates.Add(GetCert());

            try
            {
                objEnrolRef.EnrollUser(request);
                Session["ExternalId"] = enrolment.ExternalId;
                //shtohet perdoruesi ne db


                using (Models.RS_DemoEntities1 dc = new Models.RS_DemoEntities1())
                {
                    try
                    {
                        User usr = new User();
                        usr.nr_tel = enrolment.PhoneNumber;
                        usr.username = (enrolment.FirstName +"."+ enrolment.LastName).ToLower();
                        usr.password = usr.username + "123";
                        usr.externalid = enrolment.ExternalId;
                        dc.Users.Add(usr);
                        dc.SaveChanges();
                        
                   }
                    catch
                    {
                        ViewBag.ErrorMessage = "Error";
                    }
                }



                ViewBag.Message = "Enrollment is completed successfully!";
            }
            catch (SoapException ex)
            {

               
                String mesazhi  = ex.Detail.InnerText.ToString();
                String[] parts = mesazhi.Split(':');
                ViewBag.ErrorMessage = parts[1];
            }
        
            //InvokeService(enrolment);
            
            return View();
            


        }
        */
        public void GetgeneralTermsAndConditions(string externalid)
        {

            SigningService.SigningService TermsAndConditions = new SigningService.SigningService();

            var sth = TermsAndConditions.GetGeneralTermsAndConditions(externalid);
            


            return;
        }

        public ActionResult DisplayPDF()
        {
            string externalid = Session["ExternalId"] as String;
            //externalid = "12345";
            SigningService.SigningService TermsAndConditions = new SigningService.SigningService();
            byte[] byteArray = TermsAndConditions.GetGeneralTermsAndConditions(externalid).FileTermsAndConditions;

            return File(byteArray, "application/pdf");
        }

        public ActionResult SignRequest()
        {
            string externalid = Session["ExternalId"] as String;
           // externalid = "12345";
            SigningService.SigningRequest signingRequest = new SigningService.SigningRequest();
            signingRequest.ApplicationId = "1";
            signingRequest.ExternalId = externalid;
            SigningService.DocumentInfo doc = new SigningService.DocumentInfo();
            signingRequest.Documents[0] = doc;



            return View();
        }

        [HttpPost]
        public ActionResult UploadFiles()
        {
            string externalid = Session["ExternalId"] as String;
            List<Dokumentat> dokumentat = Session["dok_uploads"] as List<Dokumentat>;

            if (dokumentat == null)
            {
                dokumentat = new List<Dokumentat>();
            }



            if (ModelState.IsValid)

            {   //iterating through multiple file collection 
                int count = Request.Files.Count;
                int statusi = 1;
                    
                    HttpPostedFileBase file = Request.Files[Request.Files.Count-1];



                    string datatani = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                    var InputFileName = Path.GetFileName(file.FileName);
                    var ServerSavePath = Path.Combine(Server.MapPath("~/UploadedFiles/") + externalid + datatani + InputFileName);
                    var ServerSavePathdest = Path.Combine(Server.MapPath("~/UploadedFiles/dest") + externalid + datatani + InputFileName);

                    //Save file to server folder  
                    file.SaveAs(ServerSavePath);
                    

                    
              
                    PdfReader pdfReader = new PdfReader(ServerSavePath);
                    Dokumentat doku = new Dokumentat();

                    int nr_of_signatures = SignaturePositionHelper.GetNumberofSignatures(pdfReader);
                    doku.content = System.IO.File.ReadAllBytes(ServerSavePath);
                    string signo = SignatureCostumise.GetPropertyByName(ServerSavePath, "NrSignatures");

                    if (signo != null )
                    {
                            int nr_sign_allowed = Convert.ToInt32(signo);

                            if (nr_sign_allowed <= nr_of_signatures)
                            {
                                statusi = 3;

                              return Json(new { success = false, status = statusi, responseText = "" }, JsonRequestBehavior.AllowGet);

                            }
                    }
                        doku.filename = file.FileName;
                    
                    if (nr_of_signatures == 0)
                        {
                            doku.isSigned = false;
                            statusi = 2;
                            //per kete dokument qe nuk eshte i firmosur do te kerkohet informacion mbi popupin

                        }
                        else
                        {
                            statusi = 1;
                            doku.isSigned = true;

                        }

                dokumentat.Add(doku);
                Session["dok_uploads"] = dokumentat;

                return Json(new { success = true, status = statusi, responseText = "" }, JsonRequestBehavior.AllowGet);

            }

            return Json(new { success = false, responseText = "Ngarkoni dokumentat që dëshironi të nënshkruani!" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public  ActionResult UploadFilesforSigning()
        {
            
            string externalid = Session["ExternalId"] as String;

            if (externalid == "")
            {
                RedirectToAction("Index", "Login");
            }

            SigningService.SigningRequest signingRequest = new SigningService.SigningRequest();

            List<byte[]> dokumentat = new List<byte[]>();
            List<IntermediateSignature> intermediat = new List<IntermediateSignature>();
            List<int> signumbers = Session["signumbers"] as List<int>;



            //externalid = "12345";
            signingRequest.ApplicationId = "1";
            signingRequest.ExternalId = externalid;
            byte[] hasharray = Session["HashTermsAndConditions"] as byte[];
            //signingRequest.MasterHash = hasharray;
            int i = 0;
            int count_unsigned = 0;
            SHA256 sh256 = new SHA256CryptoServiceProvider();
            int count = 0;
             List<Dokumentat> doks = Session["dok_uploads"] as List<Dokumentat>;

            if (doks == null)
            {
                return Json(new { success = false, responseText = "Ngarkoni dokumentat që dëshironi të nënshkruani!" }, JsonRequestBehavior.AllowGet);
            }
            //Ensure model state is valid  
            if (ModelState.IsValid)
                
            {   //iterating through multiple file collection 
                count = doks.Count;
                signingRequest.Documents = new SigningService.DocumentInfo[count];
                for (int k = 0; k<doks.Count; k++)
                {
                 
                    byte[] file = doks[i].content;
                    //Checking file is available to save.  
                    if (file != null)
                    {
                        string datatani = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                        var InputFileName = Path.GetFileName(doks[i].filename);
                        var ServerSavePath = Path.Combine(Server.MapPath("~/UploadedFiles/") + externalid+datatani + InputFileName);
                        var ServerSavePathdest = Path.Combine(Server.MapPath("~/UploadedFiles/dest") + externalid + datatani + InputFileName);

                        //Save file to server folder  
                        System.IO.File.WriteAllBytes(ServerSavePath, file);
                        //assigning file uploaded status to ViewBag for showing message to user.
                        int nrofSignaturesAllowedInput = 1;
                        if (doks[i].isSigned == false)
                        {
                           
                            if (count_unsigned >= signumbers.Count)
                            {
                                nrofSignaturesAllowedInput = signumbers[signumbers.Count-1];
                            }
                            else
                            {
                                nrofSignaturesAllowedInput = signumbers[count_unsigned++];
                            }

                            count_unsigned++;
                        }
                       
                        PdfReader pdfReader = new PdfReader(ServerSavePath);
                        PdfStamper stamper = new PdfStamper(pdfReader, new FileStream(ServerSavePathdest, FileMode.Create, FileAccess.Write, FileShare.None));
                        var info = pdfReader.Info;
                        SigningService.DocumentInfo doc1 = new SigningService.DocumentInfo();
                        SigningService.DocumentInfo[] listdoc = new SigningService.DocumentInfo[100];
                        Byte[] doc_pdf = null;
                        int numberOfPages = pdfReader.NumberOfPages;
                        //vendoset metadata e nr te lejuar te firmave ne dok
                        if (SignaturePositionHelper.GetNumberofSignatures(pdfReader) == 0)
                        {
                            string signo1 = SignatureCostumise.GetPropertyByName(ServerSavePath, "NrSignatures");
                            if (signo1==null || signo1 == "")
                            {
                                info.Add("NrSignatures", nrofSignaturesAllowedInput.ToString());
                                stamper.MoreInfo = info;
                                stamper.FormFlattening = true;
                                if (nrofSignaturesAllowedInput > 12)
                                {
                                    int pages = 0;

                                    if (nrofSignaturesAllowedInput > 12)
                                    {

                                        pages = (nrofSignaturesAllowedInput - 12) / 60 + 1;
                                    }
                                    Rectangle mediabox = pdfReader.GetPageSize(1);
                                    for (int j = 0; j < pages; j++)
                                    {
                                        stamper.InsertPage(numberOfPages + 1, mediabox);
                                    }

                                    stamper.Close();

                                    doc_pdf = System.IO.File.ReadAllBytes(ServerSavePathdest);
                                }
                                else
                                {
                                    stamper.Close();
                                    doc_pdf = System.IO.File.ReadAllBytes(ServerSavePathdest);
                                }
                            }
                            else
                            {
                                stamper.Close();
                                doc_pdf = System.IO.File.ReadAllBytes(ServerSavePath);
                            }
                           
                        }
                        else
                        {
                            stamper.Close();
                            doc_pdf = System.IO.File.ReadAllBytes(ServerSavePath);
                        }

                        PdfReader pdfReader1 = new PdfReader(ServerSavePathdest);
                         numberOfPages = pdfReader1.NumberOfPages;
                        
                       int numberofsignatures = pdfReader1.AcroFields.GetSignatureNames().Count;


                        

                        int signo = Convert.ToInt32(SignatureCostumise.GetPropertyByName(ServerSavePathdest, "NrSignatures"));
                        var signProv = SignatureProviderFactory.GetSignatureProvider();
                        var scs = SignaturePositionHelper.CaculatePosition(numberofsignatures, signo, numberOfPages);

                        TimeZone localZone = TimeZone.CurrentTimeZone;
                        DateTime currentDate = DateTime.Now;
                        string emri = Session["Name"] as string;
                        string mbiemeri = Session["Lastname"] as string;
                        string organisation = Session["Organisation"] as string;
                        string title = Session["Title"] as string;


                        string teksti = emri + " " + mbiemeri + " @Date " + DateTime.Now.ToString("dd-MM-yyyy") + " " + DateTime.Now.ToString("hh:mm:ss") + " " + "+02'00'" + "  @" + title + "@" + organisation;
                       // string teksti = "";
                        teksti = teksti.Replace("@", "" + System.Environment.NewLine);
                  
                        var intermediateSig = signProv.GetIntermediateSignature(doc_pdf, teksti , scs);
                        //doc1.Data = doc_pdf;
                        doc1.DataType = SigningService.DocumentInfoDocumentDataType.DocumentHash;
                        doc1.DataTypeSpecified = true;
                        doc1.Title = InputFileName;
                        doc1.Hash = intermediateSig.Hash;
                        dokumentat.Add(doc_pdf);
                        intermediat.Add(intermediateSig);
                        signingRequest.Documents[i] = doc1;

                        //kalkulimi i masterhashit

                        if (i < count - 1)
                        {
                            sh256.TransformBlock(intermediateSig.Hash, 0, intermediateSig.Hash.Length, intermediateSig.Hash, 0);
                        }
                        else
                        {
                            sh256.TransformFinalBlock(intermediateSig.Hash, 0, intermediateSig.Hash.Length);
                        }

                        stamper.Close();
                        pdfReader.Close();
                        pdfReader1.Close();

                        i++;
                        System.IO.File.Delete(ServerSavePath);
                        System.IO.File.Delete(ServerSavePathdest);

                    }
                 
                }

                Session["list_dokumentat"] = dokumentat;
                Session["intermediats"] = intermediat;
           
            }

            if (count > 0)
            {
                SigningService.SigningService TermsAndConditions = new SigningService.SigningService();
                TermsAndConditions.ClientCertificates.Add(GetCert());
                var sth = TermsAndConditions.GetGeneralTermsAndConditions(externalid);
                byte[] hasendt = sth.HashTermsAndConditions;
                signingRequest.MasterHash = sh256.Hash;
                Session["signingrequest"] = signingRequest;

                TC doc = new TC();
                doc.type = "0";
                doc.Data = sth.FileTermsAndConditions;

                if (signingRequest == null)
                {
                    return Json(new { success = false, responseText = "Ngarkoni dokumentat që dëshironi të nënshkruani!" }, JsonRequestBehavior.AllowGet);
                }

                
                SigningService.SigningService signingservice = new SigningService.SigningService();
                signingservice.ClientCertificates.Add(GetCert());
                var response = signingservice.InitiateSigning(signingRequest, hasendt);
                Session["sessionid"] = response;
                Session["dok_uploads"] = null;
                return Json(new { success = true, responseText = "Success!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Session["dok_uploads"] = null;
                Session["signumbers"] = null;
                return Json(new { success = true, responseText = "Ju lutem, ngarkoni dokumentet që doni të nënshkruani!" }, JsonRequestBehavior.AllowGet);
                
            }
            

        }

        public static byte[] Bytes(HttpPostedFileBase result)
        {
            var length = result.InputStream.Length; //Length: 103050706
            MemoryStream target = new MemoryStream();
            result.InputStream.CopyTo(target); // generates problem in this line
            byte[] data = target.ToArray();
            return data;
        }

        public static byte[] Bytes1(HttpPostedFileBase result)
        {
            byte[] data;

            using (var reader = new BinaryReader(result.InputStream))
            {
                data = reader.ReadBytes(result.ContentLength);
            }
            return data;
        }

        public ActionResult AuthorizeSigning(string code)
        {
            
            string certPath = "C:\\" + "sslroot.cer";
            string sessionid = Session["sessionid"] as string;

            string applicationid = "1";

            SigningService.SigningService signingservice = new SigningService.SigningService();
            signingservice.ClientCertificates.Add(GetCert());
            SigningService.AuthorizationRequest request = new SigningService.AuthorizationRequest();
            SigningService.SigningResponse response = new SigningService.SigningResponse();
            request.ApplicationId = applicationid;
            request.SessionId = sessionid;
            request.Code = code;
            try {
                response = signingservice.AuthorizeSigning(request);

           /*     foreach (SigningService.DocumentSignature document in response.Signatures)
                {
                    return File(document.Data.Data, "application/pdf", "Signed.pdf");
                    
                }
                */
            }
            catch (SoapException ex)
            {
                string externalid = Session["ExternalId"] as String;
                shto_loge(externalid, "AuthorizeSigning", ex.Detail.InnerText.ToString(), 2);
                return Json(new { success = false, responseText = "Ka ndodhur një gabim gjatë verifikimit të kodit!" }, JsonRequestBehavior.AllowGet);
            }
            

            return Json(new { success = true, responseText = "Success!" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSignedTermsAndConditions()
        {
           
            string externalid = Session["ExternalId"] as String;
           // externalid = "12345";
            SigningService.SigningService signingservice = new SigningService.SigningService();
            signingservice.ClientCertificates.Add(GetCert());

            byte[] response = signingservice.GetSignedTermsAndConditions(externalid);
            return File(response, "application/pdf", "Signed.pdf");

        }

        public ActionResult GetSignaturesAsync()
        {
            

            var outputStream = new MemoryStream();
            string sessionid = Session["sessionid"] as String;
            string externalid = Session["ExternalId"] as String;

            List<byte[]> dokumentat =  Session["list_dokumentat"]  as List<byte[]>;
            List<IntermediateSignature> intermediat = Session["list_dokumentat"] as List<IntermediateSignature>;
            intermediat = Session["intermediats"] as  List<IntermediateSignature>;
            //externalid = "12345";
            SigningService.SigningService signingservice = new SigningService.SigningService();
            signingservice.ClientCertificates.Add(GetCert());
            SigningService.SigningResponse response = new SigningService.SigningResponse();
            int i = 0;
            response = signingservice.GetSignaturesAsync(sessionid);
            var signProv = SignatureProviderFactory.GetSignatureProvider();
          

            using (MemoryStream ms = new MemoryStream())
            {
                using (var archive = new ZipArchive(ms, ZipArchiveMode.Create, true))
                {
                    foreach (SigningService.DocumentSignature document in response.Signatures)
                    {
                        
                        //get only first file
                        var entry = archive.CreateEntry("Signed"+i + ".pdf", CompressionLevel.Fastest);
                        using (var zipStream = entry.Open())
                        {
                            
                          
                            var size = document.Signature.Length;
                            try
                            {
                                string datatani = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                                var sigPdf = signProv.EmbedSignature(intermediat[i].BlankSignature, document.Signature, intermediat[i].SignatureFieldName);
                                var ServerSavePathsrc = Path.Combine(Server.MapPath("~/UploadedFiles/src") + externalid + datatani + i + ".pdf");
                                System.IO.File.WriteAllBytes(ServerSavePathsrc,sigPdf);
                                var ServerSavePathdst = Path.Combine(Server.MapPath("~/UploadedFiles/dst") + externalid + datatani + i + ".pdf");
                                SignatureCostumise.Sign(ServerSavePathsrc, ServerSavePathdst);
                                byte[] dok_nenshkruar = System.IO.File.ReadAllBytes(ServerSavePathdst);
                                zipStream.Write(dok_nenshkruar, 0, dok_nenshkruar.Length);

                                System.IO.File.Delete(ServerSavePathsrc);
                                System.IO.File.Delete(ServerSavePathdst);
                            }
                            catch (Exception ex)
                            {
                                
                                shto_loge(externalid, "EmbedSignature", ex.Message.ToString(), 2);
                            }
                            
                          
                            
                        }
                        i++;
                        //return File(document.Signature, "application/pdf", "Signed.pdf");

                    }
                }
                Session["signingrequest"] = null;
                Session["signumbers"] = null;
                if (ms != null)
                {
                    shto_loge(externalid, "GetSignaturesAsync", "Shkarkimi me sukses", 1);
                }
                else
                {
                    shto_loge(externalid, "GetSignaturesAsync", "Dokumenti zip eshte null", 2);
                }
                
                return File(ms.ToArray(), "application/zip", "Dokumentat e nenshkruara.zip");
            }

        }

       public X509Certificate GetCert()
            
        {
            X509Certificate Certificate = null;
            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);

            store.Open(OpenFlags.ReadOnly);

            try

            {

                // Try to find the certificate

                // based on its common name

                X509Certificate2Collection Results =

                store.Certificates.Find(X509FindType.FindBySerialNumber, "410fd81df13f98b8b87c59", false);

                if (Results.Count == 0)

                    return null;

                else

                    Certificate = Results[0];

            }

            finally

            {

                store.Close();
              

            }

            return Certificate;
        }
        [HttpPost]
        public ActionResult ChangePass(RNE_Enrolment usr)
        {
            string externalid = Session["ExternalId"] as String;
            if (usr.Password.Trim(' ') == "")
            {
                ModelState.AddModelError("error", "Lutem plotësoni fjalëkalimin");
                return View(usr);
            }
            if (kontrollo_password(usr.Password.Trim(' ')) == false)
            {
                ModelState.AddModelError("error", "Passwordi nuk përmbush kompleksitetin e nevojshëm");
                return View(usr);
            }
            if (usr.Password != usr.ConfirmPassword)
            {
                ModelState.AddModelError("error", "Fjalëkalimet nuk përputhen");
                return View(usr);
            }
            using (Models.RNE2 dc = new Models.RNE2())
            {
                try
                {
                    RNE_Enrolment updateusr = new RNE_Enrolment();
                    updateusr = dc.RNE_Enrolment.Where(a => a.externalid == externalid).FirstOrDefault();

                    if (updateusr != null)
                    {
                        updateusr.Password = CreateMD5(usr.Password);
                        dc.SaveChanges();
                        ModelState.AddModelError("error", "Fjalëkalimi u ndryshua me sukses!");
                        updateusr.Password = "";
                        return View(updateusr);
                    }
                  

                }
                catch
                {
                    ViewBag.ErrorMessage = "Error";
                    return View(usr);
                }
                return View(usr);
            }
        }

        [HttpPost]
        public JsonResult SendSign()
        {
            try
            {

                SigningService.SigningRequest signingRequest = new SigningService.SigningRequest();
                signingRequest = Session["signingrequest"] as SigningService.SigningRequest;

                if (signingRequest==null)
                {
                    return Json(new { success = false, responseText = "Ngarkoni dokumentat që dëshironi të nënshkruani!" }, JsonRequestBehavior.AllowGet);
                }

                string externalid = Session["ExternalId"] as String;
                SigningService.SigningService signingservice = new SigningService.SigningService();
                SigningService.SigningService TermsAndConditions = new SigningService.SigningService();
                TermsAndConditions.ClientCertificates.Add(GetCert());
                var sth = TermsAndConditions.GetGeneralTermsAndConditions(externalid);
                byte[] hasendt = sth.HashTermsAndConditions;
                

                signingservice.ClientCertificates.Add(GetCert());
                
                 var response = signingservice.InitiateSigning(signingRequest, hasendt);
                Session["sessionid"] = response;


                return Json(new { success = true, responseText = "Kodi u dërgua me sukses!" }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new { success = false, responseText = "Ndodhi një problem gjatë procesit të nënshkrimit!" }, JsonRequestBehavior.AllowGet);
            }
        }

        public static bool kontrollo_password(string password)
        {

            if (password.Length < 12)
            {
                return false;
            }
                
                if (!Regex.Match(password, "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$", RegexOptions.ECMAScript).Success)
            {
                return false;
            }
           /* if (!Regex.Match(password, @"/[a-z]/", RegexOptions.ECMAScript).Success &&
                  Regex.Match(password, @"/[A-Z]/", RegexOptions.ECMAScript).Success)
            {
                return false;
            }
                   
                if (!Regex.Match(password, @"/.[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]/", RegexOptions.ECMAScript).Success)
            {
                return false;
            }
            */

            return true;
            
        }
        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString().Substring(0, 30);
            }
        }

        public static bool shto_loge(string externalid,string metoda, string pershkrimi, byte sukses)
        {
           
                using (Models.RNE2 dc = new Models.RNE2())
                {
                     var v = (from person in dc.RNE_Person
                             join enrolment in dc.RNE_Enrolment.Where(enrolment => enrolment.externalid == externalid) on person.IDPerson equals enrolment.IDPerson
                             select new { RNE_Enrolment = enrolment }).FirstOrDefault();
            
                if (v != null)
                {



                    using (Models.RNE2 dc1 = new Models.RNE2())
                    {
                        try
                        {
                            RNE_Loge_Nenshkrimi loge = new RNE_Loge_Nenshkrimi();

                            loge.Data = DateTime.Now;
                            loge.IDRNE_Enrolment = v.RNE_Enrolment.IDRNE_Enrolment;
                            loge.Metoda = metoda;
                            loge.Pershkrimi = pershkrimi;
                            loge.Sukses = sukses;
                            dc1.RNE_Loge_Nenshkrimi.Add(loge);
                            dc1.SaveChanges();

                            return true;


                        }
                        catch (Exception ex)
                        {

                            return false;
                        }

                    }   
                }
            }
            return false;
        }

        PdfDictionary GetAsDictAndMarkUsed(PdfStamperImp writer, PdfDictionary dictionary, params PdfName[] names)
        {
            PRIndirectReference reference = null;
            foreach (PdfName name in names)
            {
                if (dictionary != null)
                {
                    dictionary = dictionary.GetDirectObject(name) as PdfDictionary;
                    if (dictionary != null)
                    {
                        if (dictionary.IndRef != null)
                            reference = dictionary.IndRef;
                    }
                }
            }
            if (reference != null)
                writer.MarkUsed(reference);

            return dictionary;
        }

        [HttpPost]
        public ActionResult SaveSignatureNumbersPerDocument(int number)
        {

            List<int> signumbers = Session["signumbers"] as List<int>;

            if (signumbers == null)
            {
                signumbers = new List<int>();
                

            }

            signumbers.Add(number);
            Session["signumbers"] = signumbers;

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);

        }

        public JsonResult IsSigned(HttpPostedFileBase file)
        {


            return Json(new { success = true, responseText = "Kodi u dërgua me sukses!" }, JsonRequestBehavior.AllowGet);
        }
    }
}