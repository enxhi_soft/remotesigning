﻿using RemoteSigning.Models;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace RemoteSigning.Controllers
{
    public class ProfileController : Controller
    {
        // GET: Profile
        public ActionResult Index()
        {
            ViewBag.Message = "Profile";

            string externalid = "";
            if (Session["ExternalId"] != null)
            {
                externalid = Session["ExternalId"] as String;
            } else
            {
                return Redirect("~/Login/Index");
            }

            using(RNE2 db = new RNE2())
            {
                var user = (from useri in db.RNE_Person
                            join enrolment in db.RNE_Enrolment
                            on useri.IDPerson equals enrolment.IDPerson
                            where enrolment.externalid == externalid
                            select useri)
                        .FirstOrDefault();

                ViewBag.user = user;
            }
            return View();
        }

      
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(Profile profile)
        {
            string externalid = "";
            if (Session["ExternalId"] != null)
            {
                externalid = Session["ExternalId"] as String;
            }
            else
            {
                return Redirect("~/Login/Index");
            }

            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "dicka shkoi gabim");
                //return Redirect("Profile/Index");
            } else
            {
                using (RNE2 db = new RNE2())
                {
                    RNE_Enrolment enrolmenti = new RNE_Enrolment();
                    enrolmenti = db.RNE_Enrolment.Where(o => o.externalid == externalid).FirstOrDefault();

                    RNE_Person person = new RNE_Person();
                    person = db.RNE_Person.Where(o => o.IDPerson == enrolmenti.IDPerson).FirstOrDefault();
                    person.Nr_cel = profile.RNE_Person.Nr_cel;
                    person.Email = profile.RNE_Person.Email;

                    // Beji uncomment per momentin egzistojne dy externalid te njejta te ktij useri tek kjo tabele prnd po e le koment
                    // Shtojme PhoneNumber ne RSS_01.User
                   using (RSS_01 rs = new RSS_01())
                    {
                        User user = new User();

                        // Kam bere lidhjen me tabeles RNE.RNE_Enrolment me RSS_01.User me celes ExternalId
                        user = rs.Users.Where(o => o.ExternalId == externalid).FirstOrDefault();
                        user.PhoneNumber = profile.RNE_Person.Nr_cel;

                        //Debug.Write(user);
                        rs.SaveChanges();
                    } 
                   

                    if (profile.RNE_Enrolment.Password != null && profile.RNE_Enrolment.Password == profile.RNE_Enrolment.ConfirmPassword)
                    {
                        var passwordi = CreateMD5(profile.RNE_Enrolment.Password);

                        enrolmenti.Password = passwordi;
                        enrolmenti.ModifiedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                }
            }
            return Redirect("Profile/Index");
        }

        
        //Check if user send the correct generated number and in time
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public string Check(string kodi)
        {
            string generatedNumber;
            string dateTimeout;

            generatedNumber = Session["GeneratedNumber"].ToString();
            dateTimeout = Session["NumberTimeout"].ToString();

            var startTime = DateTime.Parse(dateTimeout);
            var endTime = DateTime.Now;

            TimeSpan span = endTime.Subtract(startTime);

            var numberTime = span.TotalSeconds;

            Debug.Write(numberTime);

            // Kodi ka 4 ore kohe
            if ((int)numberTime < 120 &&
                    generatedNumber != null &&
                    kodi.ToString() == generatedNumber.ToString()
                )
            {
                return "true";
            }
            else if((int)numberTime >= 120)
            {
                return "expired";
            } else if (generatedNumber == null)
            {
                return "null";
            }
            else
            {

                return "false";
            }
        }

        [HttpPost]
        public string GenerateNumber()
        {
            string externalid = Session["ExternalId"] as String;
            string numriGjeneruar = SendSMS(externalid);

            Session["GeneratedNumber"] = numriGjeneruar;
            Session["NumberTimeout"] = DateTime.Now;

            string time = Session["NumberTimeout"].ToString();

            return numriGjeneruar;
        }


        // Send SMS function
        public static string SendSMS(string eid)
        {
            using (RNE2 dc = new RNE2())
            {
                /*     var phonenumber = (from user in rs.Users
                                        where user.ExternalId == eid
                                        select user.PhoneNumber).FirstOrDefault();
                                        */

                var phonenumber = (from user in dc.RNE_Enrolment
                                   where user.externalid == eid
                                   select user.telefon_nr).FirstOrDefault();

                Random generator = new Random();
            string rand = generator.Next(0, 999999).ToString("D6");

            string message = "Kodi juaj i autorizimit ne Platformen e Nenshkrimit Elektronik eshte: "+rand;

            StringBuilder xmlStr = new StringBuilder();
            xmlStr.Append(@"<GovTalkMessage xmlns=""http://www.govtalk.gov.uk/CM/envelope""><EnvelopeVersion>2.0</EnvelopeVersion><Header><MessageDetails><Class>NAIS_SEND_SMS</Class><Qualifier>request</Qualifier><Function>submit</Function><CorrelationID/><Transformation>XML</Transformation></MessageDetails><SenderDetails><IDAuthentication><SenderID>PKIAC12FG</SenderID><Authentication><Method>clear</Method><Value>PK!Pr0d@123</Value></Authentication></IDAuthentication></SenderDetails></Header><GovTalkDetails><Keys><Key Type=""SpokeName"">SVC_7_IND</Key></Keys></GovTalkDetails><Body><soap:Envelope xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"" xmlns:sms=""http://SMSGateWay.al.vodafone""><soap:Header /><soap:Body><sms:SendSMS><sms:username>user</sms:username><sms:password>pass</sms:password><sms:originator>const</sms:originator><sms:msisdn>");
            xmlStr.Append(phonenumber);
            xmlStr.Append("</sms:msisdn><sms:message>");
            xmlStr.Append(message);
            xmlStr.Append("</sms:message></sms:SendSMS></soap:Body></soap:Envelope></Body></GovTalkMessage>");

            string url = "https://e-albania.gov.al/gg/submission";

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlStr.ToString());

            string response;
            response = HTTPSubmit(doc, url);

            return rand;
            }
        }


        public static string HTTPSubmit(XmlDocument GovTalkMessage, string URL)
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
            HttpWebRequest Req;
            HttpWebResponse Resp;
            Req = (HttpWebRequest)WebRequest.Create(URL);
            Req.Method = "POST";
            Req.ContentType = "text/xml";
            StreamWriter streamW = new StreamWriter(Req.GetRequestStream());
            GovTalkMessage.Save(streamW);
            streamW.Close();
            //Req.Timeout = 50000;
            Resp = (HttpWebResponse)Req.GetResponse();
            Stream respStream = Resp.GetResponseStream();
            StreamReader streamR = new StreamReader(respStream, Encoding.GetEncoding("utf-8"));
            string ResponseText = streamR.ReadToEnd();
            streamR.Close();
            Resp.Close();
            return ResponseText;
        }


        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString().Substring(0, 30);
            }
        }

    }
}