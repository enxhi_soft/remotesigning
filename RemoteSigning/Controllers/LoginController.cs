﻿using RemoteSigning.Models;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Xml;

namespace RemoteSigning.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Index(string msg = "")
        {
            ViewBag.Msg = msg;
            return View();
        }
        // GET: Login
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult View1()
        {
            return View();
        }
        public ActionResult ForgotPassword()
        {
            return View();
        }
        public ActionResult ConfirmCelNr()
        {
            string nr_cel = Session["nr_cel"] as string;
            var aStringBuilder = new StringBuilder(nr_cel);

            aStringBuilder.Remove(6, 4);
            aStringBuilder.Insert(6, "XXXX");
            ViewBag.Mesazh_nr_cel = aStringBuilder.ToString();
            return View();
        }



        /*
        [HttpPost]
        public ActionResult Login(User usr)
        {
            using (Models.RS_DemoEntities1 dc = new Models.RS_DemoEntities1())
            {
                try
                {
                    var v = dc.Users.Where(a => a.username == usr.username && a.password == usr.password).FirstOrDefault();

                    if (v != null)
                    {
                        Session["ExternalId"] = v.externalid;

                        return RedirectToAction("Sign", "Home");
                    }
                    else
                    {
                        ViewBag.Message = "Wrong username or password";
                        return View();
                    }

               }
                catch
                {
                    ViewBag.ErrorMessage = "Error";
                    return View();
                }
            }
            return View();
        } */

        [HttpPost]
        public ActionResult Index(RNE_Enrolment enrolmenti)
        {
            if (enrolmenti.NID == "" || enrolmenti.NID == null)
            {
                ViewBag.Message = "Ju lutem, plotësoni kodin e përdoruesit (NID)!";
                return View();
            }
            if (enrolmenti.Password == "" || enrolmenti.Password == null)
            {
                ViewBag.Message = "Ju lutem, plotësoni fjalëkalimin!";
                return View();
            }
            int lloji = 0;
            string lloji_regjistrimit = Request["lloji_apl"];
            if (lloji_regjistrimit== "qytetar")
            {
                lloji = 1;
            }
            else if (lloji_regjistrimit == "punonjës")
            {
                lloji = 2;
            }
            else
            {
                lloji = 1;
            }
            using (Models.RNE2 dc = new Models.RNE2())
            {
                try
                {
                    string pass = CreateMD5(enrolmenti.Password);
                    var v = (from person in dc.RNE_Person.Where(person => person.NID == enrolmenti.NID)
                             join enrolment in dc.RNE_Enrolment.Where(enrolment => enrolment.Password == pass && enrolment.lloji_regjistrimit==lloji && enrolment.Fshire ==2 ) on person.IDPerson equals enrolment.IDPerson
                             orderby enrolment.IDRNE_Enrolment descending
                             select new {RNE_Enrolment = enrolment, RNE_Person = person }).FirstOrDefault();
                            

                    if (v != null)
                    {
                       if (v.RNE_Enrolment.Aktiv == 1)
                        {

                        
                        Session["ExternalId"] = v.RNE_Enrolment.externalid;
                        Session["Name"] = v.RNE_Person.Emri;
                        Session["Lastname"] = v.RNE_Person.Mbiemri;
                        Session["Title"] = v.RNE_Enrolment.Title;
                        Session["Organisation"] = v.RNE_Enrolment.Organisation;
                        Session["idexternal"] = v.RNE_Enrolment.IDRNE_Enrolment;

                        if (v.RNE_Enrolment.Ndrysho_Password == 1)
                        {
                            Session["lloji_autorizimit"] = "2";
                            ViewBag.Message = "";
                            return RedirectToAction("ChangePass", "Login");
                        }
                        else
                        {
                            return RedirectToAction("SignNew", "Home");
                        }
                        }
                       else
                        {
                            ViewBag.Message = "Llogaria juaj është bllokuar. Lutem kontaktoni me administratorin e sistemit!";
                            return View();
                        }
                    }
                    else
                    {
                        ViewBag.Message = "NID ose fjalëkalim i pasaktë!";

                        //tenativat per login 
                        string tentativa = Session["tentativa"] as String;
                        int tentimi = 0;
                        if (tentativa==null || tentativa == "")
                        {
                            tentimi++;
                            Session["tentativa"] = "" + tentimi;
                        }
                        else
                        {
                            tentimi = Convert.ToInt32(Session["tentativa"] as string) + 1;
                            Session["tentativa"] = "" + tentimi;
                        }
                        
                        if (tentimi >= 3)
                        {
                            //nese jane realizuar 3 ose me shume tentativa do te bllokohet llogaria
                            if (blloko_llogarine(enrolmenti.NID, lloji) == true)
                            {
                                ViewBag.Message = "Llogaria juaj është bllokuar. Lutem kontaktoni me administratorin e sistemit!";
                                Session["tentativa"] = "0";
                                return View();
                            }
                        }

                      return View();
                    }

                }
                catch (Exception ex)
                {
                    ViewBag.Message = "Error " + ex.Message;
                    return View();
                }
            }
            return View();
        }
        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString().Substring(0,30);
            }
        }

        [HttpPost]
        public ActionResult ChangePass(RNE_Enrolment usr)
        {
            ViewBag.Message = "";
            string externalid = Session["ExternalId"] as String;
            if (usr.Password.Trim(' ') == "")
            {
                ViewBag.Message = "Lutem plotësoni fjalëkalimin";
                return View(usr);
            }
            if (kontrollo_password(usr.Password.Trim(' ')) == false)
            {
                ViewBag.Message = "Passwordi nuk përmbush kompleksitetin e nevojshëm";
             
                return View(usr);
            }
            if (usr.Password != usr.ConfirmPassword)
            {
                ViewBag.Message = "Fjalëkalimet nuk përputhen";
                return View(usr);
            }
            using (Models.RNE2 dc = new Models.RNE2())
            {
                try
                {
                    RNE_Enrolment updateusr = new RNE_Enrolment();
                    updateusr = dc.RNE_Enrolment.Where(a => a.externalid == externalid).FirstOrDefault();

                    
                        if (updateusr != null)
                        {
                      
                            if (updateusr.Ndrysho_Password == 1)
                            {
                                //Dergon sms me kodin e konfirmimit
                                //GenerateNumber();

                                //Ruan pass e vendosur ne session dhe kete vlere e perdor tek SaveFirstLoginPass() me ajax nqs kodi i autorizimit eshte i sakte ruhet ky password
                                TempData["pass"] = usr.Password;
                                return RedirectToAction("AuthorizeSms", "Login");
                            }

                       else
                        {
                            TempData["pass"] = usr.Password;
                            SaveFirstLoginPass();
                        }
                        ModelState.AddModelError("error", "Fjalëkalimi u ndryshua me sukses!");

                            //TempData["passw"] = usr.Password;
                            //updateusr.Password = CreateMD5(usr.Password);
                            //updateusr.Ndrysho_Password = 2;

                            //dc.SaveChanges();
                           // ModelState.AddModelError("error", "Fjalëkalimi u ndryshua me sukses!");

                            updateusr.Password = "";
                        // if (Session["lloji_autorizimit"] as string == "1")
                        // {
                        //Session["mesazhi_fillimi"] = "Fjalëkalimi juaj u ndryshua me sukses. Lutem, logohuni më poshtë me fjalëkalimin e ri!";
                        string mesazh = "Fjalëkalimi juaj u ndryshua me sukses. Lutem, logohuni më poshtë me fjalëkalimin e ri!";
                        return RedirectToAction("Index", "Login", new { msg = mesazh });

                      //  }
                      //  else
                       // {
                      //      return RedirectToAction("SignNew", "Home");
                      //  }
                           

                        }


                }
                catch
                {
                    ViewBag.ErrorMessage = "Ndodhi një problem gjatë ndryshimit të fjalëkalimit";
                    return View(usr);
                }
                return View(usr);
            }
        }

        public ActionResult ChangePass()
        {
            ViewBag.Message = "";
            string externalid = Session["ExternalId"] as String;

            using (Models.RNE2 dc = new Models.RNE2())
            {
                try
                {

                    var v = (from person in dc.RNE_Person
                             join enrolment in dc.RNE_Enrolment.Where(enrolment => enrolment.externalid == externalid) on person.IDPerson equals enrolment.IDPerson
                             select new { RNE_Enrolment = enrolment }).FirstOrDefault();

                    if (v != null)
                    {
                        

                        v.RNE_Enrolment.Password = "";
                        Session["ExternalId"] = v.RNE_Enrolment.externalid;

                        //if (v.RNE_Enrolment.Ndrysho_Password == 1)
                        //{
                           

                            // Ne rastin kur ndrysho_password eshte 1 degojme sms me kodin dhe presim perdoruesin te vendose kodin  
                            // Send sms with the code
                        //    GenerateNumber();
                        //    Session["Chng1"] = 1;
                        //    return RedirectToAction("AuthorizeSms", "Login");
                        //}

                        return View(v.RNE_Enrolment);
                    }
                    else
                    {
                        ViewBag.Message = "Wrong username or password";
                        return View();
                    }

                }
                catch
                {
                    ViewBag.ErrorMessage = "Error";
                    return View();
                }
            }

        }

        public bool blloko_llogarine(string  nidi, int lloji)
        {

            using (Models.RNE2 dc = new Models.RNE2())
            {
                try
                {
                   
                    RNE_Enrolment updateusr = new RNE_Enrolment();
                    var up  = (from person in dc.RNE_Person.Where(person => person.NID == nidi)
                                         join enrolment in dc.RNE_Enrolment.Where(enrolment => enrolment.lloji_regjistrimit == lloji && enrolment.Fshire==2) on person.IDPerson equals enrolment.IDPerson
                                         select new { RNE_Enrolment = enrolment}).FirstOrDefault();
                   
                    if (up != null)
                    {
                        updateusr = up.RNE_Enrolment;

                        if (updateusr != null)
                        {
                            updateusr.Aktiv = 2;
                            dc.SaveChanges();
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }
                    


                }
                catch
                {
             
                    return false;
                }
                return true;
            }

        }
        public static bool kontrollo_password(string password)
        {

            if (password.Length < 12)
            {
                return false;
            }

            if (!Regex.Match(password, "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$", RegexOptions.ECMAScript).Success)
            {
                return false;
            }
            /* if (!Regex.Match(password, @"/[a-z]/", RegexOptions.ECMAScript).Success &&
                   Regex.Match(password, @"/[A-Z]/", RegexOptions.ECMAScript).Success)
             {
                 return false;
             }

                 if (!Regex.Match(password, @"/.[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]/", RegexOptions.ECMAScript).Success)
             {
                 return false;
             }
             */

            return true;

        }

        // Get
        public ActionResult AuthorizeSms()
        {
            ViewBag.Message = "";
            //ViewBag.smsAuth = 0;
            GenerateNumber();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AuthorizeSms(string kodi)
        {
            if(ConfirmSms(kodi))
            {
                if (Session["lloji_autorizimit"] as string == "1")
                {
                    SaveFirstLoginPass1();
                    string mesazh = "Fjalëkalimi juaj u ndryshua me sukses. Lutem, logohuni më poshtë me fjalëkalimin e përkohshëm të dërguar me anë të sms!";
                    return RedirectToAction("Index", "Login", new { msg = mesazh });

                }
                else
                {
                    SaveFirstLoginPass();
                    return RedirectToAction("SignNew", "Home");
                }
                //
                return View();
            }
            else
            {
                ViewBag.Message = "Kodi eshte vendosur gabim!";
                return View();
            }
        }



        [HttpPost]
        public bool ConfirmSms(string kodi)
        {
            string generatedNumber;
            string dateTimeout;

            generatedNumber = Session["GeneratedNumber"].ToString();
            dateTimeout = Session["NumberTimeout"].ToString();

            var startTime = DateTime.Parse(dateTimeout);
            var endTime = DateTime.Now;

            TimeSpan span = endTime.Subtract(startTime);

            var numberTime = span.TotalSeconds;

            Debug.Write(numberTime);

            // Kodi ka 4 ore kohe
            if ((int)numberTime < 14400 &&
                    generatedNumber != null &&
                    kodi.ToString() == generatedNumber.ToString()
                )
            {
                ViewBag.smsAuth = 1;
                return true;
            }
            else if ((int)numberTime >= 14400)
            {
                return false;
            }
            else
            {
                return false;
            }
        }


        //Ruan passwordin pass logimit te pare
        [HttpPost]
        public bool SaveFirstLoginPass()
        {
            string externalid = Session["ExternalId"] as String;


            using (Models.RNE2 dc = new Models.RNE2())
            {
                RNE_Enrolment updateusr = new RNE_Enrolment();
                updateusr = dc.RNE_Enrolment.Where(a => a.externalid == externalid).FirstOrDefault();


                if (updateusr != null)
                {
                    string passwordi = TempData["pass"].ToString();

                    updateusr.Password = CreateMD5(passwordi);
                    updateusr.Ndrysho_Password = 2;
                    updateusr.ModifiedOn = DateTime.Now;
                    updateusr.Aktiv = 1;
                    dc.SaveChanges();
                    return true;
                }
            }
                return false;
        }
        public bool SaveFirstLoginPass1()
        {
            string externalid = Session["ExternalId"] as String;

            string password = Guid.NewGuid().ToString("N").ToLower()
                      .Replace("1", "").Replace("o", "").Replace("0", "")
                      .Substring(0, 10);

            using (Models.RNE2 dc = new Models.RNE2())
            {
                RNE_Enrolment updateusr = new RNE_Enrolment();
                updateusr = dc.RNE_Enrolment.Where(a => a.externalid == externalid).FirstOrDefault();


                if (updateusr != null)
                {


                    updateusr.Password = CreateMD5(password);
                    updateusr.Ndrysho_Password = 1;
                    updateusr.Aktiv = 1;
                    updateusr.ModifiedOn = DateTime.Now;

                    dc.SaveChanges();

                    SendSMS1(externalid, password);
                    return true;
                }
            }
            return false;
        }



        /**
         *Gjeneron numer 6 shifror dhe dergon sms ne nr e tel te personit te loguar
         */
        [HttpPost]
        public string GenerateNumber()
        {
            string externalid = Session["ExternalId"] as String;
            //string externalid = "100212";

            string numriGjeneruar = SendSMS(externalid);

            Session["GeneratedNumber"] = numriGjeneruar;
            Session["NumberTimeout"] = DateTime.Now;

            return numriGjeneruar;
        }


        // Send SMS function
        public static string SendSMS(string eid)
        {
            using (RNE2 dc = new RNE2())
            {
                /*  var phonenumber = (from user in rs.Users
                                     where user.ExternalId == eid
                                     select user.PhoneNumber).FirstOrDefault();
                                     */
                var phonenumber = (from user in dc.RNE_Enrolment
                                   where user.externalid == eid
                                   select user.telefon_nr).FirstOrDefault();

                Random generator = new Random();
                string rand = generator.Next(0, 999999).ToString("D6");

                string message = "Kodi juaj i autorizimit ne Platformen e Nenshkrimit Elektronik eshte: " + rand;

                StringBuilder xmlStr = new StringBuilder();
                xmlStr.Append(@"<GovTalkMessage xmlns=""http://www.govtalk.gov.uk/CM/envelope""><EnvelopeVersion>2.0</EnvelopeVersion><Header><MessageDetails><Class>NAIS_SEND_SMS</Class><Qualifier>request</Qualifier><Function>submit</Function><CorrelationID/><Transformation>XML</Transformation></MessageDetails><SenderDetails><IDAuthentication><SenderID>PKIAC12FG</SenderID><Authentication><Method>clear</Method><Value>PK!Pr0d@123</Value></Authentication></IDAuthentication></SenderDetails></Header><GovTalkDetails><Keys><Key Type=""SpokeName"">SVC_7_IND</Key></Keys></GovTalkDetails><Body><soap:Envelope xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"" xmlns:sms=""http://SMSGateWay.al.vodafone""><soap:Header /><soap:Body><sms:SendSMS><sms:username>user</sms:username><sms:password>pass</sms:password><sms:originator>const</sms:originator><sms:msisdn>");
                xmlStr.Append(phonenumber);
                xmlStr.Append("</sms:msisdn><sms:message>");
                xmlStr.Append(message);
                xmlStr.Append("</sms:message></sms:SendSMS></soap:Body></soap:Envelope></Body></GovTalkMessage>");

                string url = "https://e-albania.gov.al/gg/submission";

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlStr.ToString());

                string response;
                response = HTTPSubmit(doc, url);

                return rand;
            }
        }

        public static void SendSMS1(string eid, string password)
        {
            using (RNE2 dc = new RNE2())
            {
                /*  var phonenumber = (from user in rs.Users
                                     where user.ExternalId == eid
                                     select user.PhoneNumber).FirstOrDefault();
                                     */
                var phonenumber = (from user in dc.RNE_Enrolment
                                   where user.externalid == eid
                                   select user.telefon_nr).FirstOrDefault();

                Random generator = new Random();

                string message = "Fjalëkalimi juaj i përkohshëm ne Platformen e Nenshkrimit Elektronik eshte: " + password;

                StringBuilder xmlStr = new StringBuilder();
                xmlStr.Append(@"<GovTalkMessage xmlns=""http://www.govtalk.gov.uk/CM/envelope""><EnvelopeVersion>2.0</EnvelopeVersion><Header><MessageDetails><Class>NAIS_SEND_SMS</Class><Qualifier>request</Qualifier><Function>submit</Function><CorrelationID/><Transformation>XML</Transformation></MessageDetails><SenderDetails><IDAuthentication><SenderID>PKIAC12FG</SenderID><Authentication><Method>clear</Method><Value>PK!Pr0d@123</Value></Authentication></IDAuthentication></SenderDetails></Header><GovTalkDetails><Keys><Key Type=""SpokeName"">SVC_7_IND</Key></Keys></GovTalkDetails><Body><soap:Envelope xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"" xmlns:sms=""http://SMSGateWay.al.vodafone""><soap:Header /><soap:Body><sms:SendSMS><sms:username>user</sms:username><sms:password>pass</sms:password><sms:originator>const</sms:originator><sms:msisdn>");
                xmlStr.Append(phonenumber);
                xmlStr.Append("</sms:msisdn><sms:message>");
                xmlStr.Append(message);
                xmlStr.Append("</sms:message></sms:SendSMS></soap:Body></soap:Envelope></Body></GovTalkMessage>");

                string url = "https://e-albania.gov.al/gg/submission";

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlStr.ToString());

                string response;
                response = HTTPSubmit(doc, url);

                //return rand;
            }
        }

        public static string HTTPSubmit(XmlDocument GovTalkMessage, string URL)
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
            HttpWebRequest Req;
            HttpWebResponse Resp;
            Req = (HttpWebRequest)WebRequest.Create(URL);
            Req.Method = "POST";
            Req.ContentType = "text/xml";
            StreamWriter streamW = new StreamWriter(Req.GetRequestStream());
            GovTalkMessage.Save(streamW);
            streamW.Close();
            //Req.Timeout = 50000;
            Resp = (HttpWebResponse)Req.GetResponse();
            Stream respStream = Resp.GetResponseStream();
            StreamReader streamR = new StreamReader(respStream, Encoding.GetEncoding("utf-8"));
            string ResponseText = streamR.ReadToEnd();
            streamR.Close();
            Resp.Close();
            return ResponseText;
        }
        #region per_kofirminin_cel

        [HttpPost]
        public ActionResult ForgotPassword(RNE_Enrolment enrolmenti)
        {
            int lloji = 0;
            string lloji_regjistrimit = Request["lloji_apl"];
            if (lloji_regjistrimit == "qytetar")
            {
                lloji = 1;
            }
            else if (lloji_regjistrimit == "punonjës")
            {
                lloji = 2;
            }
            else
            {
                lloji = 1;
            }
            using (Models.RNE2 dc = new Models.RNE2())
            {
                try
                {
                   
                    var v = (from person in dc.RNE_Person.Where(person => person.NID == enrolmenti.NID)
                             join enrolment in dc.RNE_Enrolment.Where(enrolment=> enrolment.lloji_regjistrimit == lloji) on person.IDPerson equals enrolment.IDPerson
                             orderby enrolment.IDRNE_Enrolment descending
                             select new { RNE_Enrolment = enrolment, RNE_Person = person }).FirstOrDefault();


                    if (v != null)
                    {
                        Session["nr_cel"] = v.RNE_Enrolment.telefon_nr;
                        Session["ExternalId"] = v.RNE_Enrolment.externalid;
                        return RedirectToAction("ConfirmCelNr", "Login");
                    }
                    else
                    {
                        ViewBag.MessageForgot = "Nuk u gjet asnjë llogari me të dhënat e vendosura!";
                        return View();
                    }
                }
                catch(Exception ex)
                {
                    ViewBag.MessageForgot = "Ka ndodhur një gabim në sistemin fundor";
                    return View();

                }
            }
        }

        [HttpPost]

        public ActionResult ConfirmCelNr(string kodi)
        {
            string nr_cel = Session["nr_cel"] as string;
            var aStringBuilder = new StringBuilder(nr_cel);
            var aStringBuilder_panjohur = new StringBuilder(nr_cel);

            aStringBuilder.Remove(8, 4);
            aStringBuilder.Insert(8, kodi);

            aStringBuilder_panjohur.Remove(6, 4);
            aStringBuilder_panjohur.Insert(6, "XXXX");

            if  (nr_cel != aStringBuilder.ToString())
            {
                ViewBag.MsgCelNr = "Shifrat e vendosura nuk jane të sakta!";
                ViewBag.Mesazh_nr_cel = aStringBuilder_panjohur.ToString();
                return View();
            }
            else
            {
                Session["lloji_autorizimit"] = "1"; //per autorizim sms per ndryshim pass
                return RedirectToAction("AuthorizeSms", "Login");
            }
        }
        #endregion
    }
}
