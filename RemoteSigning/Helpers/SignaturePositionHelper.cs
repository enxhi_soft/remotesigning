﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Paperless.DotNetSdk;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace RemoteSigning.Helpers
{
    public class SignaturePositionHelper
    {
        public int lowerLeftX { get; set; }
        public int lowerLeftY { get; set; }
        public int upperRightX { get; set; }
        public int upperRightY { get; set; }
        public int pageNumber { get; set; }



        public static int GetNumberofSignatures(PdfReader reader)
        {
            AcroFields acroFields = reader.AcroFields;

            List<string> signaturenames = acroFields.GetSignatureNames();

            return signaturenames.Count();

        }


        public static PDFSigningContextSettings CaculatePosition(int signingTimes, int maxAllowedSignatures, int nrPages)
        {

            int pageToPlaceSignature = nrPages;
            int lowerX = 100;
            int lowerY = 100;
            int upperX = 200;
            int upperY = 150;
            switch (maxAllowedSignatures) {
                case int n when (n < 12):
                    decimal ax = signingTimes / 4;
                    if (signingTimes <= 12 )
                    {
                        pageToPlaceSignature = nrPages;
                       
                        if (ax < 3)
                        {
                            lowerX = (signingTimes - Convert.ToInt32(Math.Floor(ax)) * 4) * 100;
                            lowerY = Convert.ToInt32(Math.Floor(ax)) * 50;
                            upperX = lowerX + 100;
                            upperY = lowerY + 50;
                        }
                       
                    }
                
                    break;
                 case int n when(n > 12):
                    int reallastpage = nrPages - ((maxAllowedSignatures - 12) / 60 + 1);
                    if (signingTimes <= 12)
                    {
                        pageToPlaceSignature = reallastpage;
                        decimal ax2 = signingTimes / 4;
                        if (ax2 < 3)
                        {
                            lowerX = (signingTimes - Convert.ToInt32(Math.Floor(ax2)) * 4) * 100;
                            lowerY = Convert.ToInt32(Math.Floor(ax2)) * 50;
                            upperX = lowerX + 100;
                            upperY = lowerY + 50;
                        }

                    }
                    else
                    {
                        decimal ax1 = signingTimes - 12 / 4;
                        if (ax1 < 6)
                        {
                            lowerX = ((signingTimes - 12) - Convert.ToInt32(Math.Floor(ax1)) * 4) * 100;
                            lowerY = Convert.ToInt32(Math.Floor(ax1)) * 50;
                            upperX = lowerX + 100;
                            upperY = lowerY + 50;
                        }
                        pageToPlaceSignature = reallastpage + (signingTimes - 12) / 60;
                    }
                   

                    


                    break;

                default:
                    break;

            }

            var scs = new PDFSigningContextSettings
            {
                Reason = "Nënshkrim Elektronik",
                Location = "Shqipëri",
                SignatureType = Paperless.DotNetSdk.SignatureTypeEnum.CUSTOM_SIGNATURE,
                VisibilityOptions = new PDFVisibilityOptions
                {
                    SignatureRectangle = new PDFSignatureRectangle(lowerX, lowerY, upperX, upperY),
                    //SignatureRectangle = new PDFSignatureRectangle(100 , 100, 200 , 150),
                    SignaturePageNumber = pageToPlaceSignature,
                    FontSize = 8
                },
            };
            return scs;
        }


    }

  
}