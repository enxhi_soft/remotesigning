﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.security;
using Org.BouncyCastle.X509;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace RemoteSigning.Helpers
{
    public class SignatureCostumise
    {
        public static void Sign(string SRC, string DEST)
        {
          
            using (PdfReader pdfReader = new PdfReader(SRC))
            using (PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(DEST, FileMode.Create, FileAccess.Write), '\0', true))
            {
                AcroFields acroFields = pdfStamper.AcroFields;

                //foreach (String signatureName in acroFields.GetSignatureNames())
                // {
                List<string> signaturenames = acroFields.GetSignatureNames();
                string signatureName = signaturenames.LastOrDefault();
                    PdfPKCS7 pkcs7 = acroFields.VerifySignature(signatureName);
                    X509Certificate signerCert = pkcs7.SigningCertificate;
                    String signerName = CertificateInfo.GetSubjectFields(signerCert).GetField("CN");
                    string[] words = signerName.Split(' ');
                    AcroFields.Item field = acroFields.GetFieldItem(signatureName);
                    
                    for (int i = 0; i < field.Size; i++)
                    {
                        PdfDictionary widget = field.GetWidget(i);

                        Rectangle rect = PdfReader.GetNormalizedRectangle(widget.GetAsArray(PdfName.RECT));
 

                        PdfAppearance appearance = PdfAppearance.CreateAppearance(pdfStamper.Writer, rect.Width, rect.Height);
                        string URL = HttpRuntime.AppDomainAppPath;
                        string imageFilePath = URL +"images\\1.png";

                        iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageFilePath);
                        jpg.Alignment = iTextSharp.text.Image.ALIGN_MIDDLE;

                        jpg.ScaleToFit(100, 100);
                        jpg.SetAbsolutePosition(10,-25);

                        appearance.AddImage(jpg, false);
                        //kolona e pare
                        ColumnText columnText = new ColumnText(appearance);
                        //columnText.AddElement(jpg);
                        Chunk chunk = new Chunk();
                      
                        BaseFont bf = BaseFont.CreateFont(URL +"css\\fonts\\Myriad Pro Regular.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.BOLD);
                        //chunk.SetTextRenderMode(PdfContentByte.TEXT_RENDER_MODE_FILL_STROKE, 1, BaseColor.BLACK); 
                        chunk.Append(words[0]);

                        columnText.AddElement(new Paragraph(chunk));
                        chunk.Font = font;
                        chunk = new Chunk();
                        chunk.Append(words[1]);
                        columnText.AddElement(new Paragraph(chunk));
                        chunk.Font = font;
                        chunk = new Chunk();
                        chunk.Font = font;
                        columnText.AddElement(new Paragraph(chunk));
                        columnText.SetSimpleColumn(0, 0, rect.Width - rect.Width / 2, rect.Height);
                        columnText.Go();
                        //kolona e dyte
                        ColumnText columnText1 = new ColumnText(appearance);
                        Chunk chunk1 = new Chunk();
                        BaseFont bf1 = BaseFont.CreateFont(URL + "css\\fonts\\Myriad Pro Regular.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        iTextSharp.text.Font font1 = new iTextSharp.text.Font(bf1, 6, iTextSharp.text.Font.NORMAL);

                        //chunk1.SetSkew(0, 12);
                        /* chunk1.Append("Digitally Signed by:");
                         columnText1.AddElement(new Paragraph(chunk1));
                         chunk1.Font = font1;
                          chunk1 = new Chunk();
                         */


                        chunk1.Font = font1;
                        //chunk1.SetTextRenderMode(PdfContentByte.TEXT_RENDER_MODE_FILL_STROKE, 1, BaseColor.BLACK);
                        string teksti = "Digitally signed by " + signerName + "@Date: " + DateTime.Now.ToString("yyyy.MM.dd") + " " + DateTime.Now.ToString("hh:mm:ss") + " " + "+02'00'";
                        teksti = teksti.Replace("@", "" + System.Environment.NewLine);
                        chunk1.Append(teksti);


                        columnText1.AddElement(new Paragraph(chunk1));
                        Console.Write(rect.Width);
                        columnText1.SetSimpleColumn(rect.Width - rect.Width / 2, 0, rect.Width, rect.Height);
                        columnText1.Go();

                        PdfDictionary xObjects = GetAsDictAndMarkUsed((PdfStamperImp)pdfStamper.Writer, widget, PdfName.AP, PdfName.N, PdfName.RESOURCES, PdfName.XOBJECT, PdfName.FRM, PdfName.RESOURCES, PdfName.XOBJECT);
                        xObjects.Put(PdfName.N2, appearance.IndirectReference);
                   // }
                }
            }
        }
        static PdfDictionary  GetAsDictAndMarkUsed(PdfStamperImp writer, PdfDictionary dictionary, params PdfName[] names)
        {
            PRIndirectReference reference = null;
            foreach (PdfName name in names)
            {
                if (dictionary != null)
                {
                    dictionary = dictionary.GetDirectObject(name) as PdfDictionary;
                    if (dictionary != null)
                    {
                        if (dictionary.IndRef != null)
                            reference = dictionary.IndRef;
                    }
                }
            }
            if (reference != null)
                writer.MarkUsed(reference);

            return dictionary;
        }

        public static string GetPropertyByName(string filePath, string propertyName)
        {
            if (String.IsNullOrEmpty(filePath) || !System.IO.File.Exists(filePath) || String.IsNullOrEmpty(propertyName)) { return null; }
            Dictionary<string, string> propertyInfo = GetPdfProperties(filePath);
            foreach (KeyValuePair<string, string> property in propertyInfo)
            {
                if (property.Key == propertyName) { return property.Value; }
            }
            return null; 
        }

        public static Dictionary<string, string> GetPdfProperties(string filePath)
        {
            Dictionary<string, string> propertyInfo = null;

            using (PdfReader reader = new PdfReader(filePath))
            {
                propertyInfo = reader.Info;
                reader.Close();
            }

            return propertyInfo;
        }
    }
}