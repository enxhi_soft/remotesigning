﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RemoteSigning.Helpers
{
    public class Dokumentat
    {
        public string filename { get; set; }
        public byte[]  content { get; set; }
        public bool isSigned { get; set; }
    }
}