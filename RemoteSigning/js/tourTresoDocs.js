﻿var tour = new Tour({
    backdrop: true,
    animation: false,
    // backdropContainer: '#wrapper',
    template: "<div class='popover tour tour-treso-docs'><div class='arrow'></div><span class='stop_tour' style='float:right; color:white; margin: 7px 7px 0 0; cursor: pointer'><i class='ti-close'/></span><h3 class='popover-title'></h3><div class='popover-content'></div><nav class='popover-navigation text-right'><div class='btn-group'><button class='btn btn-default' data-role='prev'>Â« PrÃ©cÃ©dent</button><button class='btn btn-l2c4a next' data-role='next'>Suivant Â»</button></div><button class='btn btn-success btn-end' data-role='end'>J\'ai compris</button></nav></div>",
    onShown: function (tour) {
        $(".next").removeClass("disabled");
        $('.popover .btn-default.disabled').prop('disabled', false);
        // var stepElement = getTourElement(tour);
        // $(stepElement).after($('.tour-step-background'));
        // $(stepElement).after($('.tour-backdrop'));
    },
    onEnd: function (tour) {
        $.ajax({
            url: $("div.bankTransactions").data('url-end-tuto'),
        }).done(function (dataReturn) {
        }).fail(function () {
            alert("Erreur lors de la sauvegarde du tutorial");
        });
    },
    steps: [
        {
            element: "#tablist_AF li:nth-child(1)",
            title: "1/6 Factures et relevÃ©s bancaires",
            content: "<p>AccÃ©dez Ã  vos factures fournisseurs et clients, ainsi qu'Ã  vos diffÃ©rents relevÃ©s bancaires professionnels en cliquant sur les 3 premiers onglets.</p>",
            placement: "bottom",
        },
        {
            element: "#tablist_AF li:nth-child(4)",
            title: "2/6 Factures antÃ©rieures",
            content: "<p>AccÃ©dez Ã  vos factures antÃ©rieures directement depuis cet onglet.</p>",
            placement: "bottom",
        },
        {
            element: "#tablist_AF li:nth-child(5)",
            title: "3/6 Documents divers",
            content: "<p>Consultez les diffÃ©rents documents vous concernant et Ã©tablis par notre cabinet.</p>",
            placement: "bottom",
        },
        {
            element: ".tablist_action",
            title: "4/6 DÃ©poser vos documents",
            content: "<p>Pour dÃ©poser vos factures, relevÃ©s et autres documents, cliquez sur le lien \"DÃ©poser\" : dans la fenÃªtre s'ouvrant, vous pouvez choisir la catÃ©gorie concernÃ©e puis glisser/dÃ©poser vos documents (maximum : 100 fichiers simultannÃ©s).</p>",
            placement: "left",
        },
        {
            element: "#formTri",
            title: "5/6 PÃ©riode sÃ©lectionnÃ©e",
            content: "<p>Vous pouvez filtrer vos factures selon le mois et l'annÃ©e souhaitÃ©e. Vous pouvez Ã©galement afficher les documents d'une annÃ©e complÃ¨te sans sÃ©lectionner de mois.</p>",
            placement: "bottom",
        },
        {
            element: "#search_datas",
            title: "6/6 Barre de Recherche",
            content: "<p>Pour rechercher une facture par son nom, vous pouvez utiliser ce champ de recherche.</p>",
            placement: "bottom",
        }
    ]
});

/*function getTourElement(tour){
    // return tour._steps[tour._current].element
    return tour._options.steps[tour._current].element
}*/

jQuery(document).ready(function ($) {

    $(document).on('click', '.popover .btn-default.disabled, .stop_tour', function () {
        tour.end();
    });

});