﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RemoteSigning.Models
{
    public class User
    {
        public long ID { get; set; }

        [StringLength(50)]
        public string ExternalId { get; set; }

        [StringLength(50)]
        public string GuidKey { get; set; }

        [StringLength(50)]
        public string Cnp { get; set; }

        [StringLength(4000)]
        public string IdCardSerial { get; set; }

        [StringLength(4000)]
        public string IdCardNumber { get; set; }

        public DateTime? IdCardIssueDate { get; set; }

        [StringLength(4000)]
        public string IdCardIssuer { get; set; }

        public DateTime? IdCardExpirationDate { get; set; }

        [StringLength(4000)]
        public string FirstName { get; set; }

        [StringLength(4000)]
        public string LastName { get; set; }

        public byte[] CustomerIdPhoto { get; set; }

        [StringLength(4000)]
        public string PhoneNumber { get; set; }

        [StringLength(4000)]
        public string Email { get; set; }

        [StringLength(4000)]
        public string Address { get; set; }

        [StringLength(4000)]
        public string Country { get; set; }

        [StringLength(4000)]
        public string District { get; set; }

        [StringLength(4000)]
        public string Locality { get; set; }

        [StringLength(4000)]
        public string Sector { get; set; }

        [StringLength(4000)]
        public string Block { get; set; }

        [StringLength(4000)]
        public string Entrance { get; set; }

        [StringLength(4000)]
        public string Apartment { get; set; }

        [StringLength(4000)]
        public string Street { get; set; }

        [StringLength(4000)]
        public string Nr { get; set; }

        [StringLength(4000)]
        public string ZIPCode { get; set; }

        [StringLength(50)]
        public string EnrollmentCode { get; set; }

        public byte[] HashTermsAndConditions { get; set; }

        public byte[] SignedTermsAndConditionsFile { get; set; }

        public DateTime RegisteredTime { get; set; }

        [StringLength(4000)]
        public string Roles { get; set; }

        public long? SubscriberUser_ID { get; set; }

        public long? Group_ID { get; set; }

        public long? SubscriberProfile_ID { get; set; }

        public int? Valability { get; set; }

        public long? Subscriber_ID { get; set; }
    }
}