namespace RemoteSigning.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RNE_Loge_Nenshkrimi
    {
        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal IDRNE_Loge_Nenshkrimi { get; set; }

        public byte? Sukses { get; set; }

        [StringLength(5000)]
        public string Pershkrimi { get; set; }

        [StringLength(50)]
        public string Metoda { get; set; }

        public DateTime? Data { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? IDRNE_Enrolment { get; set; }

        public virtual RNE_Enrolment RNE_Enrolment { get; set; }
    }
}
