namespace RemoteSigning.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class RNE2 : DbContext
    {
        public RNE2()
            : base("name=RNE2")
        {
        }

        public virtual DbSet<RNE_Enrolment> RNE_Enrolment { get; set; }
        public virtual DbSet<RNE_Loge_Nenshkrimi> RNE_Loge_Nenshkrimi { get; set; }
        public virtual DbSet<RNE_Person> RNE_Person { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RNE_Enrolment>()
                .Property(e => e.IDRNE_Enrolment)
                .HasPrecision(19, 0);

            modelBuilder.Entity<RNE_Enrolment>()
                .Property(e => e.IDPerson)
                .HasPrecision(19, 0);

            modelBuilder.Entity<RNE_Enrolment>()
                .Property(e => e.externalid)
                .IsUnicode(false);

            modelBuilder.Entity<RNE_Enrolment>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<RNE_Enrolment>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<RNE_Enrolment>()
                .Property(e => e.Organisation)
                .IsUnicode(false);

            modelBuilder.Entity<RNE_Enrolment>()
                .Property(e => e.Organisation_Unit)
                .IsUnicode(false);

            modelBuilder.Entity<RNE_Enrolment>()
                .Property(e => e.telefon_nr)
                .IsUnicode(false);

            modelBuilder.Entity<RNE_Loge_Nenshkrimi>()
                .Property(e => e.IDRNE_Loge_Nenshkrimi)
                .HasPrecision(19, 0);

            modelBuilder.Entity<RNE_Loge_Nenshkrimi>()
                .Property(e => e.Pershkrimi)
                .IsUnicode(false);

            modelBuilder.Entity<RNE_Loge_Nenshkrimi>()
                .Property(e => e.Metoda)
                .IsUnicode(false);

            modelBuilder.Entity<RNE_Loge_Nenshkrimi>()
                .Property(e => e.IDRNE_Enrolment)
                .HasPrecision(19, 0);

            modelBuilder.Entity<RNE_Person>()
                .Property(e => e.IDPerson)
                .HasPrecision(19, 0);

            modelBuilder.Entity<RNE_Person>()
                .Property(e => e.Emri)
                .IsUnicode(false);

            modelBuilder.Entity<RNE_Person>()
                .Property(e => e.Mbiemri)
                .IsUnicode(false);

            modelBuilder.Entity<RNE_Person>()
                .Property(e => e.NID)
                .IsUnicode(false);

            modelBuilder.Entity<RNE_Person>()
                .Property(e => e.KrijuarNga)
                .HasPrecision(19, 0);

            modelBuilder.Entity<RNE_Person>()
                .Property(e => e.IP_Krijimit)
                .IsUnicode(false);

            modelBuilder.Entity<RNE_Person>()
                .Property(e => e.Gjinia)
                .IsUnicode(false);

            modelBuilder.Entity<RNE_Person>()
                .Property(e => e.Emertimi_fotos)
                .IsUnicode(false);

            modelBuilder.Entity<RNE_Person>()
                .Property(e => e.Prapashtesa)
                .IsUnicode(false);

            modelBuilder.Entity<RNE_Person>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<RNE_Person>()
                .Property(e => e.Nr_cel)
                .IsUnicode(false);

            modelBuilder.Entity<RNE_Person>()
                .Property(e => e.Adresa)
                .IsUnicode(false);

            modelBuilder.Entity<RNE_Person>()
                .Property(e => e.Qyteti)
                .IsUnicode(false);
        }
    }
}
