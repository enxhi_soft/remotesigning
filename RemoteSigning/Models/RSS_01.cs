namespace RemoteSigning.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class RSS_01 : DbContext
    {
        public RSS_01()
            : base("name=RSS_011")
        {
        }

        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
              .Property(e => e.PhoneNumber)
              .IsUnicode(false);
        }
    }
}
