﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace RemoteSigning.Models
{
    public class Enrolment
    {
        
        public string ExternalId { get; set; }
       
        public string Cnp { get; set; }
       
        public string IdCardSerial { get; set; }
       
        public string IdCardNumber { get; set; }

        public DateTime? IdCardIssueDate { get; set; }
      
        public string IdCardIssuer { get; set; }
   
     
        public DateTime? IdCardExpirationDate { get; set; }
  
        public string FirstName { get; set; }
       
        public string LastName { get; set; }
        /*Customer Identification Card photo*/
        
        public byte[] CustomerIdPhoto { get; set; }
       
        public string PhoneNumber { get; set; }
        
        public string Email { get; set; }
      
        public string Address { get; set; }
        /*ISO 3166-1 ,Alpha 2: two letter country code*/
       
        public string Country { get; set; }
      
        public string District { get; set; }
    
        public string Locality { get; set; }
       
        public string Sector { get; set; }

        public string Block { get; set; }
      
        public string Entrance { get; set; }
       
        public string Apartment { get; set; }
       
        public string Street { get; set; }
      
        public string Nr { get; set; }
   
        public string ZIPCode { get; set; }
      
        public string Roles { get; set; }
        
        public Dictionary<string, string> AdditionalInfo { get; set; }
       
        public string Profile { get; set; }
   
        public string Group { get; set; }
      
        public int? CertificateValidity { get; set; }
    }
}