namespace RemoteSigning.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web.Security;

    public partial class RNE_Enrolment
    {
        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal IDRNE_Enrolment { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? IDPerson { get; set; }


        [StringLength(50)]
        public string externalid { get; set; }


        [StringLength(50)]
        //[Required]
        //[MembershipPassword(
        //MinRequiredNonAlphanumericCharacters = 2,
        //MinNonAlphanumericCharactersError = "Fjalekalimi duhet te permbaje te pakten nje simbol (!, @, #, etc) dhe nje numer.",
        //ErrorMessage = "Fjalekalimi duhet te jete te pakten 12 karaktere i gjate dhe duhet te permbaje te pakten nje simbol (!, @, #, etc) dhe nje numer.",
        //MinRequiredPasswordLength = 12
        //)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public DateTime? CreatedOn { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public int? CreatedBy { get; set; }

        public int? ModifiedBy { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(50)]
        public string Organisation { get; set; }

        [StringLength(50)]
        public string Organisation_Unit { get; set; }


        public byte? Ndrysho_Password { get; set; }

        [NotMapped]
        public string NID { get; set; }

        public virtual RNE_Person RNE_Person { get; set; }

        [NotMapped]
        //[Required(ErrorMessage = "")]
        //[Compare("Password", ErrorMessage = "Fjalekalimet nuk jane te njejta")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        public int? lloji_regjistrimit { get; set; }
        public int? Aktiv { get; set; }
        public byte? Fshire { get; set; }
        public string telefon_nr { get; set; }
    }
}
