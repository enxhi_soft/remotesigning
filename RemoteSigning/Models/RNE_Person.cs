namespace RemoteSigning.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RNE_Person
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RNE_Person()
        {
            RNE_Enrolment = new HashSet<RNE_Enrolment>();
        }

        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal IDPerson { get; set; }

        [StringLength(50)]
        public string Emri { get; set; }

        [StringLength(50)]
        public string Mbiemri { get; set; }

        [StringLength(50)]
        public string NID { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? KrijuarNga { get; set; }

        public DateTime? KrijuarMe { get; set; }

        [StringLength(20)]
        public string IP_Krijimit { get; set; }

        public DateTime? Datelindje { get; set; }

        [StringLength(1)]
        public string Gjinia { get; set; }

        [Column(TypeName = "image")]
        public byte[] Foto { get; set; }

        [StringLength(500)]
        public string Emertimi_fotos { get; set; }

        [StringLength(10)]
        public string Prapashtesa { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(50)]
        public string Nr_cel { get; set; }

        [StringLength(2000)]
        public string Adresa { get; set; }

        [StringLength(50)]
        public string Qyteti { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RNE_Enrolment> RNE_Enrolment { get; set; }
    }
}
