﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace RemoteSigning.Models
{
    public class SigningRequest
    {
        // External id of the user:
   
        public string ExternalId { get; set; }
        //Id of the signing application
 
        public string ApplicationId { get; set; }
        //Documents/hashes to be signed

        public IEnumerable<DocumentInfo> Documents { get; set; }
        /*Hash value on all hashes combined. To be sent on SMS with authorization code.*/


        public byte[] MasterHash { get; set; }
    }
    public class DocumentInfo
    {
        //Document title
 
        public string Title { get; set; }
        //Content of the document

        public byte[] Data { get; set; }
        //Data type listed below
 
        public DocumentDataType DataType { get; set; }
        //Hash of the document

        public byte[] Hash { get; set; }

        public enum DocumentDataType
        {
            [EnumMember]
            DocumentHash = 0,
            [EnumMember]
            PdfDocument = 1,
            [EnumMember]
            GenericDocument = 2
        }
    }
}