﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RemoteSigning.Models
{
    public class Profile
    {
        public RNE_Person RNE_Person { get; set; }
        public User User { get; set; }
        public RNE_Enrolment RNE_Enrolment { get; set; }

        public Profile() { }

        public Profile(RNE_Person rne_person, User user, RNE_Enrolment rne_enrolment)
        {
            RNE_Person = rne_person;
            User = user;
            RNE_Enrolment = rne_enrolment;
        }
    }
}